package com.bitcoinWallet.BitcoinWalletBack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class BitcoinWalletBackApplication {

    public static void main(String[] args) {
        SpringApplication.run(BitcoinWalletBackApplication.class, args);
    }

}
