package com.bitcoinWallet.BitcoinWalletBack.controller;

import com.bitcoinWallet.BitcoinWalletBack.model.CryptoCurrency;
import com.bitcoinWallet.BitcoinWalletBack.service.CryptoCurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/crypto_currencies")
public class CryptoCurrencyController {

    @Autowired
    CryptoCurrencyService cryptoCurrencyService;

    @GetMapping(path="/{cryptoCurrencyId}")
    public ResponseEntity<CryptoCurrency> getCryptoCurrency(@PathVariable("cryptoCurrencyId") Long cryptoCurrencyId) {
        CryptoCurrency cryptoCurrency = cryptoCurrencyService.getCryptoCurrency(cryptoCurrencyId).get();
        return ResponseEntity.accepted().body(cryptoCurrency);
    }
    @GetMapping
    public ResponseEntity<List<CryptoCurrency>> getCryptoCurrencies() {
        List<CryptoCurrency> cryptoCurrencies = cryptoCurrencyService.getAllCryptoCurrencies();
        return ResponseEntity.accepted().body(cryptoCurrencies);
    }

    @DeleteMapping
    public ResponseEntity<String> deleteCryptoCurrency(@PathVariable Long cryptoCurrencyId) {
        cryptoCurrencyService.deleteCryptoCurrency(cryptoCurrencyId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<String> addCryptoCurrency(@RequestBody CryptoCurrency cryptoCurrency) {
        cryptoCurrencyService.addCryptoCurrency(cryptoCurrency);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<String> updateCryptoCurrency(@RequestBody CryptoCurrency cryptoCurrency) {
        cryptoCurrencyService.updateCryptoCurrency(cryptoCurrency);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
