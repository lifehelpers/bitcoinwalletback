package com.bitcoinWallet.BitcoinWalletBack.controller;

import com.bitcoinWallet.BitcoinWalletBack.model.UserCryptoCurrency;
import com.bitcoinWallet.BitcoinWalletBack.service.UserCryptoCurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/wallet")
public class UserCryptoCurrencyController {

    @Autowired
    UserCryptoCurrencyService userCryptoCurrencyService;

    @GetMapping(path = "/{userCryptoCurrencyId}")
    public ResponseEntity<UserCryptoCurrency> getUserCryptoCurrency(@PathVariable("userCryptoCurrencyId") Long userCryptoCurrencyId) {
        UserCryptoCurrency userCryptoCurrency = userCryptoCurrencyService.getUserCryptoCurrency(userCryptoCurrencyId).get();
        return ResponseEntity.accepted().body(userCryptoCurrency);
    }

    @GetMapping(path="/users/{userId}")
    public ResponseEntity<List<UserCryptoCurrency>> getUserCryptoCurrencies(@PathVariable("userId") Long userId) {
        List<UserCryptoCurrency> userCryptoCurrencies = userCryptoCurrencyService.getUserCryptoCurrencies(userId);
        return ResponseEntity.accepted().body(userCryptoCurrencies);
    }


    @DeleteMapping(path = "/{userCryptoCurrencyId}")
    public ResponseEntity<String> deleteUserCryptoCurrency(@PathVariable Long userCryptoCurrencyId) {
        userCryptoCurrencyService.deleteUserCryptoCurrency(userCryptoCurrencyId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Long> addUserCryptoCurrency(@RequestBody UserCryptoCurrency userCryptoCurrency) {
        return ResponseEntity.accepted().body(userCryptoCurrencyService.addUserCryptoCurrency(userCryptoCurrency));
    }

    @PutMapping
    public ResponseEntity<String> updateUserCryptoCurrency(@RequestBody UserCryptoCurrency userCryptoCurrency) {
        userCryptoCurrencyService.updateUserCryptoCurrency(userCryptoCurrency);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
