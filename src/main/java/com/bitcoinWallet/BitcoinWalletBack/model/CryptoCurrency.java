package com.bitcoinWallet.BitcoinWalletBack.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "cryptoCurrencyId")
@Entity
@Data
@Table(name = "crypto_currencies")
@NoArgsConstructor
public class CryptoCurrency {

    @Id
    @GeneratedValue
    @Column(name = "crypto_currency_id")
    private Long cryptoCurrencyId;

    public CryptoCurrency(String name, String symbol) {
        this.name = name;
        this.symbol = symbol;
    }

    @NotBlank
    @Size(min = 3, max = 100)
    private String name;

    @Column(columnDefinition = "text")
    private String symbol;
}
