package com.bitcoinWallet.BitcoinWalletBack.model;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "users_crypto_currencies")
public class UserCryptoCurrency {
    //example https://api.bitfinex.com/v1/pubticker/BTCEUR
    private static final String CRYPTO_COIN_API_BASE_URL = "https://api.bitfinex.com/v1/pubticker/";
    private static final String CRYPTO_CURRENCY_FROM_BITNEX = "USD";
    private static final String EUR_USD_API_URL = "http://free.currencyconverterapi.com/api/v5/convert?q=EUR_USD&compact=y";
    private static Double EUR_USD_VALUE;

    @Id
    @GeneratedValue
    @Column(name = "user_crypto_currency_id")
    private Long userCryptoCurrencyId;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "crypto_currency_id")
    private Long cryptoCurrencyId;

    @Column(name = "amount")
    private Double amount;

    @Temporal(TemporalType.DATE)
    @Column(name = "date_of_purchase")
    private Date dateOfPurchase;

    @Column(name = "wallet_location")
    private String walletLocation;

    private Double currentMarketValue;

    public void fetchCurrentMarketValue(String cryptoCurrencySymbol) {
        URL url;
        URLConnection request;
        fetchEurToUsd();
        try {
            synchronized (this) {
                while (EUR_USD_VALUE == null) {
                    this.wait();
                }
                try {
                    url = new URL(CRYPTO_COIN_API_BASE_URL + cryptoCurrencySymbol + CRYPTO_CURRENCY_FROM_BITNEX);
                    request = url.openConnection();
                    request.connect();
                    JsonParser jp = new JsonParser(); //from gson
                    JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent())); //Convert the input stream to a json element
                    JsonObject rootobj = root.getAsJsonObject(); //May be an array, may be an object.
                    String marketValue = rootobj.get("last_price").getAsString();
                    setCurrentMarketValue(Math.round(((Double.parseDouble(marketValue) * getAmount()) / EUR_USD_VALUE) * 100000d) / 100000d);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void fetchEurToUsd() {
        URL url;
        URLConnection request;
        Double EurToUsd = null;
        synchronized (this) {
            try {
                url = new URL(EUR_USD_API_URL);
                request = url.openConnection();
                request.connect();

                JsonParser jp = new JsonParser(); //from gson
                JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent())); //Convert the input stream to a json element
                JsonObject rootobj = root.getAsJsonObject(); //May be an array, may be an object.
                EurToUsd = Double.parseDouble(rootobj.get("EUR_USD").getAsJsonObject().get("val").getAsString());
            } catch (IOException e) {
                e.printStackTrace();
            }
            EUR_USD_VALUE = EurToUsd;
            this.notify();
        }
    }
}
