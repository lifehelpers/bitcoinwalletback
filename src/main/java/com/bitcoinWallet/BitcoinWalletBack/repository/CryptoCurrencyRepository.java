package com.bitcoinWallet.BitcoinWalletBack.repository;

import com.bitcoinWallet.BitcoinWalletBack.model.CryptoCurrency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CryptoCurrencyRepository extends JpaRepository<CryptoCurrency, Long> {
    CryptoCurrency findCryptoCurrencyByCryptoCurrencyId(Long cryptoCurrencyId);
}
