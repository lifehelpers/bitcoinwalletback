package com.bitcoinWallet.BitcoinWalletBack.repository;

import com.bitcoinWallet.BitcoinWalletBack.model.UserCryptoCurrency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserCryptoCurrencyRepository extends JpaRepository<UserCryptoCurrency, Long> {
    List<UserCryptoCurrency> findAllByUserId(Long userId);
    UserCryptoCurrency findByCryptoCurrencyId(Long cryptoCurrencyId);
}
