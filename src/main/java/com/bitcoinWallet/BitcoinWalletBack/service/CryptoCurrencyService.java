package com.bitcoinWallet.BitcoinWalletBack.service;

import com.bitcoinWallet.BitcoinWalletBack.model.CryptoCurrency;
import com.bitcoinWallet.BitcoinWalletBack.repository.CryptoCurrencyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class CryptoCurrencyService {

    @Autowired
    CryptoCurrencyRepository cryptoCurrencyRepository;

    public Optional<CryptoCurrency> getCryptoCurrency(Long cryptoCurrencyId) {
        Optional<CryptoCurrency> cryptoCurrency = cryptoCurrencyRepository.findById(cryptoCurrencyId);
        if(cryptoCurrency.isPresent()) {
            return cryptoCurrency;
        }
        throw new EntityNotFoundException("CRYPTOCURRENCY NOT FOUND");
    }

    public List<CryptoCurrency> getAllCryptoCurrencies() {
        return cryptoCurrencyRepository.findAll();
    }

    public void deleteCryptoCurrency(Long cryptoCurrencyId) {
        cryptoCurrencyRepository.deleteById(cryptoCurrencyId);
    }

    public void addCryptoCurrency(CryptoCurrency cryptoCurrency) {
        cryptoCurrencyRepository.save(cryptoCurrency);
    }

    public void updateCryptoCurrency(CryptoCurrency cryptoCurrency) {
        cryptoCurrencyRepository.save(cryptoCurrency);
    }
}
