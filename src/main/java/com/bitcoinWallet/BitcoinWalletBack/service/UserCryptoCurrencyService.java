package com.bitcoinWallet.BitcoinWalletBack.service;

import com.bitcoinWallet.BitcoinWalletBack.model.UserCryptoCurrency;
import com.bitcoinWallet.BitcoinWalletBack.repository.CryptoCurrencyRepository;
import com.bitcoinWallet.BitcoinWalletBack.repository.UserCryptoCurrencyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserCryptoCurrencyService {

    @Autowired
    UserCryptoCurrencyRepository userCryptoCurrencyRepository;
    @Autowired
    CryptoCurrencyRepository cryptoCurrencyRepository;

    public Optional<UserCryptoCurrency> getUserCryptoCurrency(Long userCryptoCurrencyId) {
        Optional<UserCryptoCurrency> userCryptoCurrency = userCryptoCurrencyRepository.findById(userCryptoCurrencyId);
        return userCryptoCurrency;
    }

    public List<UserCryptoCurrency> getUserCryptoCurrencies(Long userId) {
        List<UserCryptoCurrency> userWallet = userCryptoCurrencyRepository.findAllByUserId(userId);
        userWallet.forEach(userCryptoCurrency -> {
            String symbol = cryptoCurrencyRepository.findCryptoCurrencyByCryptoCurrencyId(userCryptoCurrency.getCryptoCurrencyId()).getSymbol();
            userCryptoCurrency.fetchCurrentMarketValue(symbol);
        });
        return userWallet;

    }

    public void deleteUserCryptoCurrency(Long userCryptoCurrencyId) {
        userCryptoCurrencyRepository.deleteById(userCryptoCurrencyId);
    }

    public Long addUserCryptoCurrency(UserCryptoCurrency userCryptoCurrency) {
        return userCryptoCurrencyRepository.save(userCryptoCurrency).getUserCryptoCurrencyId();
    }

    public void updateUserCryptoCurrency(UserCryptoCurrency userCryptoCurrency) {
        userCryptoCurrencyRepository.save(userCryptoCurrency);
    }

}
