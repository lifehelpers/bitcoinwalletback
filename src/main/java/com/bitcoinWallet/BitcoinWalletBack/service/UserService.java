package com.bitcoinWallet.BitcoinWalletBack.service;

import com.bitcoinWallet.BitcoinWalletBack.model.User;
import com.bitcoinWallet.BitcoinWalletBack.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    public Optional<User> getUser(Long userId) {
        Optional<User> user = userRepository.findById(userId);
        if (user.isPresent()) {
            return user;
        }
        throw new EntityNotFoundException("USER NOT FOUND");
    }

    public void deleteUser(Long userId) {
        userRepository.deleteById(userId);
    }

    public void addUser(User user) {
        userRepository.save(user);
    }

    public void updateUser(User user) {
        userRepository.save(user);
    }
}
