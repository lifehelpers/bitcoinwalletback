const BASE_URL = '/';

let app = new Vue({
    el: '#app',
    data: function () {
        return {
            userId: 1,
            cryptos: null,
            errorCode: "",
            crypto_form_data: {
                userCryptoCurrencyId: 0,
                cryptoCurrencyId: 2,
                walletLocation: ""
            },
            errorMessages: {
                409: "This currency already exists, am i rite?",
                500: "Something broke down, sowwy"
            },
            user_cryptos: []
        }
    },
    created: function () {
        this.crypto_form_data.dateOfPurchase = new Date().toISOString().split('T')[0];
        this.fetchCryptoCurrencies();
        this.loadData(this.userId);
    },
    methods: {
        loadData: function (userId) {
            this.loadUserData(userId);
            this.loadUserCryptoData(userId);
        },
        fetchCryptoCurrencies: function () {
            axios.get(BASE_URL + 'crypto_currencies')
                .then(cryptoData => {
                    this.cryptos = cryptoData.data;
                })
                .catch(error => {
                    console.log(error)
                });
        },
        loadUserData: function (userId) {
            axios.get(BASE_URL + 'users/' + userId)
                .then(userData => {
                    this.setUserData(userData.data)
                })
                .catch(error => {
                    console.log("no such user", error.response);
                });
        },
        loadUserCryptoData: function (userId) {
            axios.get(BASE_URL + 'wallet/users/' + userId)
                .then(userCryptoData => {
                    this.setUserWalletData(userCryptoData.data)
                })
                .catch(error => {
                    console.log("User", userId, "wallet:", error.response.data.status);
                });
        },
        deleteFromUserWallet: function (crypto) {
            const url = BASE_URL + 'wallet/' + crypto.userCryptoCurrencyId;
            axios.delete(url);
            //TODO catch
        },
        addToUserWallet: function (crypto_form_data) {
            const url = BASE_URL + 'wallet';
            crypto_form_data.userId = this.user_id;
            axios.post(url, crypto_form_data)
                .catch((error) => {
                    this.errorCode = error.response.status;
                });
            //TODO catch
        },
        setUserData: function (userData) {
            this.username = userData.username;
            this.user_id = userData.userId;
        },
        setUserWalletData: function (userCryptoData) {
            this.user_cryptos = userCryptoData;
        },
        getCryptoNameFromId: function (id) {
            const cryptoWithGivenId = this.cryptos
                .find(crypto => crypto.cryptoCurrencyId === id);
            return cryptoWithGivenId.name;
        },

        formatDate: function (date) {
            return new Date(date).toLocaleDateString();
        },
        deleteCrypto: function (crypto) {
            const cryptoName = this.getCryptoNameFromId(crypto.cryptoCurrencyId);
            this.deleteFromUserWallet(crypto);
            setTimeout(function () {
                app.loadUserCryptoData(app.userId);
            }, 1000);

        },
        addCrypto: function (crypto_form_data) {
            this.resetErrorCode();
            this.addToUserWallet(crypto_form_data);
            setTimeout(function () {
                app.loadUserCryptoData(app.userId);
            }, 1000);
        },
        resetErrorCode: function () {
            this.errorCode = "";
        },
        getErrorMessage: function () {
            return this.errorMessages[this.errorCode];
        }
    }
});
